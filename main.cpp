#include <iostream>
#include <stdio.h>
#include <ctype.h>
#include <cctype>
#include <vector>
#include <algorithm>

using namespace std;

/**
 *  Programming Challenge: Temperature Conversion Program
 *  in Celsius, Fahrenheit, and Kelvin
 *  30 March 2019 - Sean Castillo
 */


const double nineDivFive = 9.0 / 5.0;
const double fiveDivNine = 5.0 / 9.0;
const vector<string> unitVector = {"f", "c", "k", "fahrenheit", "celsius", "kelvin"};

double convertCtoF(double tempC) // F = C * (9/5) + 32
{
    return tempC * nineDivFive + 32.0;
}

double convertCtoK(double tempC) // K = C + 273.15
{
    return tempC + 273.15;
}

double convertFtoC(double tempF) // C = (F - 32) * (5/9)
{
    double c;
    c = tempF - 32.0;
    return c * fiveDivNine;
}

double convertFtoK(double tempF) // K = (F + 459.67) * (5/9)
{
    double tempK = tempF + 459.67;
    return tempK * fiveDivNine;
}

double convertKtoC(double tempK) // C = K - 273.15
{
    return tempK - 273.15;
}

double convertKtoF(double tempK) // F = C * (9/5) - 459.67
{
    return tempK * nineDivFive - 459.67;
}


/* method to print the conversions */
void printConversions(double temp, unsigned char u)
{
    string originalUnit;
    if (u == 'c') { originalUnit = "celsius";    }
    if (u == 'f') { originalUnit = "fahrenheit"; }
    else          { originalUnit = "kelvin";     }
    cout << temp << " in " << originalUnit << " is: " << endl;
    unsigned char unit = u;
    if (unit != 'c')
    {
        if (unit == 'f')
        {
            cout << convertFtoC(temp) << " degrees celsius" << endl;
        }
        else
        {
            cout << convertKtoC(temp) << " degrees celsius" << endl;
        }
    }

    if (unit != 'f')
    {
        if (unit == 'c')
        {
            cout << convertCtoF(temp) << " degrees fahrenheit" << endl;
        }
        else
        {
            cout << convertKtoF(temp) << " degrees fahrenheit" << endl;
        }
    }

    if (unit != 'k')
    {
        if (unit == 'c')
        {
            cout << convertCtoK(temp) << " kelvin" << endl;
        }
        else
        {
            cout << convertFtoK(temp) << " kelvin" << endl;
        }
    }
}

/* method to get the number of the value to convert */
double getInputTemp()
{
    double inputTemp;
    cout << "Enter a number for temperature." << endl;
    while (1)
    {
        if (cin >> inputTemp) { break; } // valid number
        else
        {
            cout << "That's not a numerical value! Try Again!" << endl;
            cin.clear();
            cin.ignore(100000, '\n');
        }
    }
    return inputTemp;
}

/* method to get the unit of the value to convert */
unsigned char getInputUnit()
{
    string inputUnit;
    unitEntryLoop:
    cout << "Enter a unit: (c)elsius, (f)ahrenheit, or (k)elvin" << endl;
    cin >> inputUnit;
    std::for_each(inputUnit.begin(), inputUnit.end(), [](char & c)
    {   c = ::tolower(c); });

    if (std::find(unitVector.begin(), unitVector.end(), inputUnit) != unitVector.end())
    {
        if      (inputUnit == "c" || inputUnit == "celsius"   ) { return 'c'; }
        else if (inputUnit == "f" || inputUnit == "fahrenheit") { return 'f'; }
        else                                                    { return 'k'; }
    }
    else
    {
        cout << "That's not a unit! Try again!" << endl;
        cin.clear();
        cin.ignore(100000, '\n');
        goto unitEntryLoop;
    }
}

/* method to check whether to run the task again */
bool checkGoAgain()
{
    string answer;

    answerLoop:

    cout << "Go again? (Y)es / (N)o" << endl;
    cin >> answer;
    std::for_each(answer.begin(), answer.end(), [](char & c)
    {   c = ::tolower(c); });
    if (answer == "y" || answer == "yes") { return 1; }
    if (answer == "n" || answer == "no" ) { return 0; }
    else
    {
        cout << "Invalid response! " << endl;
        goto answerLoop;
    }
}

/* driver */
int main()
{
    runLoop:

    // get the temperature to convert
    double inputTemp            = getInputTemp();
    unsigned char inputUnitChar = getInputUnit();

    // perform the conversions
    printConversions(inputTemp, inputUnitChar);

    // check whether to go again
    bool goAgain = checkGoAgain();
    if (goAgain) { goto runLoop;}
    else         { cout << "Ending the program." << endl; }

    return 0;
}
